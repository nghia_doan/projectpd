//
//  HomeVC.swift
//  ProjectPD
//
//  Created by nghia doan on 2/24/18.
//  Copyright © 2018 CS. All rights reserved.
//

import UIKit

class HomeVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    let reuseIdentifier = "CourseCell"
    var items = [Course]()
    
    @IBOutlet weak var courseCollectionView: UICollectionView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        items.append(Course(image: "001-blackboard", nametitle: "Maths"))
        items.append(Course(image: "007-acoustic-guitar", nametitle: "Music"))
        items.append(Course(image: "003-chemistry", nametitle: "Chemistry"))
        items.append(Course(image: "004-biology", nametitle: "Biology"))
        items.append(Course(image: "005-calculation", nametitle: "Finance"))
        items.append(Course(image: "006-desktop", nametitle: "Computer"))
        courseCollectionView.dataSource = self
        courseCollectionView.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? CourseVC {
            
            if let course = sender as? Course {
                destination.course = course
            }
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.items.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! CourseCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.courseName.text = items[indexPath.row].nametitle;
        let image: UIImage = UIImage(named: items[indexPath.row].image)!
        cell.courseImage.image = image
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let course = items[indexPath.row]
        
        performSegue(withIdentifier: "CourseSegue", sender: course)
    }


}
