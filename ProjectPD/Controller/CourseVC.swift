//
//  CourseVC.swift
//  ProjectPD
//
//  Created by nghia doan on 2/25/18.
//  Copyright © 2018 CS. All rights reserved.
//

import UIKit

class CourseVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let reuseIdentifier = "TuitorCell"
    var items = [Tuitor]()
    
    
    
    @IBOutlet weak var courseNameLbl: UILabel!
    @IBOutlet weak var courseImage: UIImageView!
    
    @IBOutlet weak var tuitorTable: UITableView!
    
    private var _course : Course!
    var course: Course {
        get {
            return _course
        } set {
            _course = newValue
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tuitorTable.delegate = self
        tuitorTable.dataSource = self
        
        courseNameLbl.text = course.nametitle
        let image: UIImage = UIImage(named: course.image)!
        courseImage.image = image
        
        items.append(Tuitor(image: "LKTui", name: "Loukaew", faculty: "IBM", year: "4th year", course: course.nametitle, availability: "Mon/Thu/Fri", rate: "500 THB"))
        items.append(Tuitor(image: "NGTui", name: "Nghia Doan", faculty: "IBM", year: "4th year", course: course.nametitle, availability: "Mon/Tue/Wed", rate: "600 THB"))
        items.append(Tuitor(image: "PTui", name: "Palm", faculty: "IBM", year: "4th year", course: course.nametitle, availability: "Mon/Tue/Fri", rate: "400 THB"))
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? TuitorCell{
            
            let tuitor = items[indexPath.row]
            
            cell.updateUI(tuitor: tuitor)
            
            return cell
            
        } else {
            return UITableViewCell()
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }

}
