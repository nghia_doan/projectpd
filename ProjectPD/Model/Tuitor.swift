//
//  Tuitor.swift
//  ProjectPD
//
//  Created by nghia doan on 2/25/18.
//  Copyright © 2018 CS. All rights reserved.
//

import UIKit

class Tuitor: NSObject {
    private var _image: String!
    private var _name: String!
    private var _faculty : String!
    private var _year: String!
    private var _course : String!
    private var _availability: String!
    private var _rate : String!
    
    var image: String {
        return _image
    }
    
    var name: String {
        return _name
    }
    var faculty: String {
        return _faculty
    }
    
    var year: String {
        return _year
    }
    var course: String {
        return _course
    }
    
    var availability: String {
        return _availability
    }
    var rate: String {
        return _rate
    }
    
    
    
    init(image: String, name: String, faculty:String,year:String,course:String,availability:String,rate:String) {
        
        _image = image
        _name = name
        _faculty = faculty
        _year = year
        _course = course
        _availability = availability
        _rate = rate
    }
    

}
