//
//  Course.swift
//  ProjectPD
//
//  Created by nghia doan on 2/24/18.
//  Copyright © 2018 CS. All rights reserved.
//

import UIKit

class Course: NSObject {
    private var _image: String!
    private var _nametitle: String!
    
    var image: String {
        return _image
    }
    
    var nametitle: String {
        return _nametitle
    }
    
    
    
    init(image: String, nametitle: String) {
        
        _image = image
        _nametitle = nametitle
    }
}
