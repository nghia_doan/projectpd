//
//  CourseCollectionViewCell.swift
//  ProjectPD
//
//  Created by nghia doan on 2/24/18.
//  Copyright © 2018 CS. All rights reserved.
//

import UIKit

class CourseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var courseName: UILabel!
    @IBOutlet weak var courseImage: UIImageView!
}
