//
//  TuitorCell.swift
//  ProjectPD
//
//  Created by nghia doan on 2/25/18.
//  Copyright © 2018 CS. All rights reserved.
//

import UIKit

class TuitorCell: UITableViewCell {

    @IBOutlet weak var imageBox: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var facultyLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var courseLbl: UILabel!
    @IBOutlet weak var availLbl: UILabel!
    @IBOutlet weak var rateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateUI(tuitor: Tuitor) {
        let image = UIImage(named: tuitor.image)
        imageBox.image = image
        nameLbl.text = tuitor.name
        facultyLbl.text = tuitor.faculty
        yearLbl.text = tuitor.year
        courseLbl.text = tuitor.course
        availLbl.text = tuitor.availability
        rateLbl.text = tuitor.rate
        
    }

}
